using System.Collections.Generic;

using FIOWeb.JsonPayloads;

namespace FIOWeb.UniverseMap
{
    public class SystemStar
    {
        // Based loosely on radius values from here: https://www.enchantedlearning.com/subjects/astronomy/stars/startypes.shtml
        private static readonly Dictionary<string, float> StarTypeToRadius = new Dictionary<string, float>
        {
            { "O", 12.0f },
            { "B", 9.00f },
            { "A", 7.50f },
            { "F", 6.25f },
            { "G", 5.00f },
            { "K", 4.00f },
            { "M", 3.00f }
        };

        public SystemStar( SystemStarPayload systemStarPayload )
        {
            SystemId = systemStarPayload.SystemId;
            NaturalId = systemStarPayload.NaturalId;

            if (systemStarPayload.NaturalId != systemStarPayload.Name)
            {
                DisplayName = $"{systemStarPayload.Name} ({systemStarPayload.NaturalId})";
            }
            else
            {
                DisplayName = systemStarPayload.NaturalId;
            }

            Radius = StarTypeToRadius.ContainsKey(systemStarPayload.Type) ? StarTypeToRadius[systemStarPayload.Type] : 1.0f;

            PositionX = systemStarPayload.PositionX;
            PositionY = systemStarPayload.PositionY;
            PositionZ = systemStarPayload.PositionZ;
        }

        public string SystemId { get; set; }
        public string NaturalId { get; set; }
        public string DisplayName { get; set; }
        public float Radius { get; set; }

        public double PositionX { get; set; }
        public double PositionY { get; set; }
        public double PositionZ { get; set; }

        public uint Color { get; set; } = 0xFFFFFF;
    }
}