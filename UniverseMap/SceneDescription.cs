using System.Collections.Generic;

namespace FIOWeb.UniverseMap
{
    public class SceneDescription
    {
        public List<SubSectorLineSegment> Lines { get; set; } = new List<SubSectorLineSegment>();
        public List<SystemStar> Stars { get; set; } = new List<SystemStar>();
        public List<FTLConnection> Connections { get; set; } = new List<FTLConnection>();
    }
}