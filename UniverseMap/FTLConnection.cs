namespace FIOWeb.UniverseMap
{
    public class FTLConnection
    {
        public string StartSystemId { get; set; }
        public double StartPositionX { get; set; }
        public double StartPositionY { get; set; }
        public double StartPositionZ { get; set; }

        public string EndSystemId { get; set; }
        public double EndPositionX { get; set; }
        public double EndPositionY { get; set; }
        public double EndPositionZ { get; set; }

        public uint Color { get; set; } = 0xFFFFFF;
    }
}