using System;
using System.Collections.Generic;
using System.Linq;

using FIOWeb.JsonPayloads;

namespace FIOWeb.UniverseMap
{
    public class SubSectorLineSegment : IEquatable<SubSectorLineSegment>
    {
        public SubSectorLineSegment(SubSectorVertex start, SubSectorVertex end)
        {
            StartPositionX = start.X;
            StartPositionY = start.Y;
            StartPositionZ = start.Z;

            EndPositionX = end.X;
            EndPositionY = end.Y;
            EndPositionZ = end.Z;
        }

        public double StartPositionX { get; set; }
        public double StartPositionY { get; set; }
        public double StartPositionZ { get; set; }

        public double EndPositionX { get; set; }
        public double EndPositionY { get; set; }
        public double EndPositionZ { get; set; }

        public bool Dashed { get; set; } = false;
        public uint Color { get; set; } = 0x292929; //0xDCD2BD;

        public bool Equals(SubSectorLineSegment other)
        {
            if (other != null)
            {
                if ((
                    StartPositionX == other.StartPositionX &&
                    StartPositionY == other.StartPositionY &&
                    StartPositionZ == other.StartPositionZ &&
                    EndPositionX == other.EndPositionX &&
                    EndPositionY == other.EndPositionY &&
                    EndPositionZ == other.EndPositionZ
                    ) 
                    ||
                    (
                    StartPositionX == other.EndPositionX &&
                    StartPositionY == other.EndPositionY &&
                    StartPositionZ == other.EndPositionZ &&
                    EndPositionX == other.StartPositionX &&
                    EndPositionY == other.StartPositionY &&
                    EndPositionZ == other.StartPositionZ
                    ))
                {
                    return true;
                }
            }

            return false;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(StartPositionX + EndPositionX, StartPositionY + EndPositionY, StartPositionZ + EndPositionZ);
        }
    }

    public class SubSectorTriangle
    {
        public SubSectorTriangle(SubSector subSector)
        {
            SubSectorId = subSector.SSId;

            LineSegments.Add(new SubSectorLineSegment(subSector.Vertices[0], subSector.Vertices[1]));
            LineSegments.Add(new SubSectorLineSegment(subSector.Vertices[0], subSector.Vertices[2]));
            LineSegments.Add(new SubSectorLineSegment(subSector.Vertices[1], subSector.Vertices[2]));
        }

        public string SubSectorId { get; set; }

        public List<SubSectorLineSegment> LineSegments { get; set; } = new List<SubSectorLineSegment>();
    }

    public class Sector
    {
        public Sector(WorldSector worldSector)
        {
            SectorId = worldSector.SectorId;
            SectorName = worldSector.Name;

            foreach(var ss in worldSector.SubSectors)
            {
                SubSectors.Add(new SubSectorTriangle(ss));
            }
        }

        public string SectorId { get; set; }
        public string SectorName { get; set; }

        public List<SubSectorTriangle> SubSectors { get; set; } = new List<SubSectorTriangle>();

        public List<SubSectorLineSegment> GetUniqueLineSegments()
        {
            return GetAllLineSegments().Distinct().ToList();
        }

        public List<SubSectorLineSegment> GetAllLineSegments()
        {
            var segments = new List<SubSectorLineSegment>();
            foreach( var ss in SubSectors)
            {
                segments.AddRange(ss.LineSegments);
            }

            return segments;
        }
    }
}