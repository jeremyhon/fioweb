using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

using FIOWeb.Models;
using FIOWeb.JsonPayloads;

using MatBlazor;
using Newtonsoft.Json;

namespace FIOWeb.Pages
{
    public partial class Burn
    {
        private List<BurnModel> BurnModels = null;
        private List<MaterialPayload> AllMaterials = null;

        private List<string> UserFilters = new List<string>();
        private List<string> TickerFilters = new List<string>();

        private string DisplayMode
        {
            get
            {
                return _DisplayMode;
            }
            set
            {
                if (_DisplayMode != value)
                {
                    _DisplayMode = value;
                    BurnModelShow bms = BurnModelShow.ShowAll;
                    if (_DisplayMode == "ShowAll")
                    {
                        bms = BurnModelShow.ShowAll;
                    }
                    else if (_DisplayMode == "ShowOnlyWorkforce")
                    {
                        bms = BurnModelShow.ShowOnlyWorkforce;
                    }
                    else if (_DisplayMode == "ShowOnlyProduction")
                    {
                        bms = BurnModelShow.ShowOnlyProduction;
                    }
                    else
                    {
                        Debug.Assert(false);
                    }

                    BurnModels.ForEach(bm => bm.Show = bms);

                    StateHasChanged();
                }
            }
        }
        private string _DisplayMode = "ShowAll";

        private bool UserDrawerVisible = false;

        private string ActiveUserName = null;

        private string UsersParam = null;
        private List<string> ActiveUserNames = null;

        private string GroupParam = null;
        private int GroupId = -1;

        protected override async Task OnInitializedAsync()
        {
            if (NavManager.TryGetQueryString<string>("UserName", out ActiveUserName))
            {

            }
            else if (NavManager.TryGetQueryString<string>("Users", out UsersParam))
            {
                var UsersSplitRes = UsersParam.Split(new String[] { "__" }, StringSplitOptions.RemoveEmptyEntries);
                ActiveUserNames = UsersSplitRes.ToList();
            }
            else if (NavManager.TryGetQueryString<string>("Group", out GroupParam))
            {
                var GroupSplitRes = GroupParam.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
                if (GroupSplitRes.Length > 0)
                {
                    int.TryParse(GroupSplitRes[0], out GroupId);
                }
            }
            else
            {
                ActiveUserName = await GlobalAppState.GetUserName();
            }

            var allMaterialsRequest = new Web.Request(HttpMethod.Get, "/material/allmaterials");
            AllMaterials = await allMaterialsRequest.GetResponseAsync<List<MaterialPayload>>();


        }

        protected override void OnInitialized()
        {
            GlobalAppState.OnChange += StateHasChanged;
        }

        private List<string> UsersAllowedToView = null;
        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (!firstRender && GlobalAppState.State == LoadState.Authenticated && BurnModels == null && AllMaterials != null)
            {
                await Refresh();
            }
        }

        private bool CannotSeeUsersData = false;

        private bool InRefresh = false;
        private async Task Refresh()
        {
            if (!InRefresh)
            {
                InRefresh = true;
                CannotSeeUsersData = false;
                BurnModels = null;
                _DisplayMode = "ShowAll";
                StateHasChanged();

                UsersAllowedToView = GlobalAppState.PermissionAllowances
                    .Where(pa => pa.StorageData && pa.BuildingData && pa.ProductionData && pa.WorkforceData)
                    .Select(pa => pa.UserName)
                    .ToList();

                if (ActiveUserName != null)
                {
                    UsersAllowedToView.RemoveAll(u => u.Equals(ActiveUserName, StringComparison.OrdinalIgnoreCase)); // Remove the current ActiveUserName from the list
                    if (ActiveUserName.ToUpper() != (await GlobalAppState.GetUserName()).ToUpper())
                    {
                        UsersAllowedToView.Insert(0, await GlobalAppState.GetUserName()); // Add ourselves to the list
                    }
                }

                UsersAllowedToView.Sort();

                string AuthToken = await GlobalAppState.GetAuthToken();
                BurnModels = await BurnModel.GetBurnModels(ActiveUserName, ActiveUserNames, GroupId, AuthToken, AllMaterials);
                if (BurnModels == null)
                {
                    CannotSeeUsersData = true;
                }

                StateHasChanged();
                InRefresh = false;
            }
        }

        public void RebuildTable()
        {


            StateHasChanged();
        }

        public void Dispose()
        {
            GlobalAppState.OnChange -= StateHasChanged;
        }

        private string SelectedUser = "";
        private async void UserSelectionChanged(string value)
        {
            UserDrawerVisible = false;
            await Task.Delay(500);

            NavManager.NavigateTo($"/burn?UserName={value}");
            ActiveUserName = value;

            await Refresh();
        }
    }
}