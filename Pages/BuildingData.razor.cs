using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using FIOWeb.Models;

namespace FIOWeb.Pages
{
    public partial class BuildingData
    {
        private string ActiveUserName = null;
        private List<BuildingDataModel> AllBuildingData = null;
        private List<BuildingDataModel> FilteredBuildingData = null;

        private bool ShowNonProductionBuildings { get; set; } = false;

        private void CheckNonProductionBuildings(bool value)
        {
            ShowNonProductionBuildings = !ShowNonProductionBuildings;
            ApplyFilters();
        }

        protected override async Task OnInitializedAsync()
        {
            GlobalAppState.OnChange += StateHasChanged;

            NavManager.TryGetQueryString<string>("UserName", out ActiveUserName);

            if (await GlobalAppState.LS_ContainsKey("BuildingDataBuildingConditionThreshold-2"))
            {
                _BuildingConditionThreshold = await GlobalAppState.LSGet_Generic<double>("BuildingDataBuildingConditionThreshold-2");
            }

            StateHasChanged();
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (!firstRender && GlobalAppState.State == LoadState.Authenticated && FilteredBuildingData == null)
            {
                if (ActiveUserName == null)
                {
                    ActiveUserName = await GlobalAppState.GetUserName();
                }

                await Refresh();
            }
        }

        private List<string> UsersAllowedToView = null;

        private bool InRefresh = false;
        private async Task Refresh()
        {
            if (!InRefresh)
            {
                InRefresh = true;
                AllBuildingData = null;
                FilteredBuildingData = null;
                StateHasChanged();

                UsersAllowedToView = GlobalAppState.PermissionAllowances.Where(pa => pa.BuildingData).Select(pa => pa.UserName).ToList();
                UsersAllowedToView.RemoveAll(u => u.Equals(ActiveUserName, StringComparison.OrdinalIgnoreCase)); // Remove the current ActiveUserName from the list
                UsersAllowedToView.Sort();
                if (ActiveUserName.ToUpper() != (await GlobalAppState.GetUserName()).ToUpper())
                {
                    UsersAllowedToView.Insert(0, await GlobalAppState.GetUserName()); // Add ourselves to the list
                }

                AllBuildingData = await BuildingDataModel.GetBuildingDataModels(ActiveUserName, await GlobalAppState.GetAuthToken());
                AllBuildingData = AllBuildingData.OrderBy(bd => bd.PlanetDisplayName).ToList();
                AllBuildingData.ForEach(bd =>
                {
                    bd.Children = bd.Children.OrderBy(c => c.BuildingTicker).ToList();
                    bd.Children.ForEach(c =>
                    {
                        c.Children = c.Children.OrderBy(c => c.MaterialTicker).ToList();
                    });
                });
                ApplyFilters();

                StateHasChanged();
                InRefresh = false;
            }
        }

        public void Dispose()
        {
            GlobalAppState.OnChange -= StateHasChanged;
        }

        private void ApplyFilters()
        {
            FilteredBuildingData = new List<BuildingDataModel>();
            AllBuildingData.ForEach((item) => FilteredBuildingData.Add((BuildingDataModel)item.Clone()));

            for (int planetIdx = FilteredBuildingData.Count - 1; planetIdx >= 0; --planetIdx)
            {
                for (int buildingIdx = FilteredBuildingData[planetIdx].Children.Count - 1; buildingIdx >= 0; --buildingIdx)
                {
                    float condition = (FilteredBuildingData[planetIdx].Children[buildingIdx].Condition != null) ? (float)FilteredBuildingData[planetIdx].Children[buildingIdx].Condition : 2.0f;
                    if (condition > (BuildingConditionThreshold / 100.0))
                    {
                        FilteredBuildingData[planetIdx].Children.RemoveAt(buildingIdx);
                        continue;
                    }
                    
                    if (FilteredBuildingData[planetIdx].Children[buildingIdx].IsCoreBaseBuilding && !ShowNonProductionBuildings)
                    {
                        FilteredBuildingData[planetIdx].Children.RemoveAt(buildingIdx);
                        continue;
                    }
                }
            }

            StateHasChanged();
        }

        private bool UserDrawerVisible = false;
        private string SelectedUser = "";
        private async void UserSelectionChanged(string value)
        {
            UserDrawerVisible = false;
            await Task.Delay(500);

            NavManager.NavigateTo($"/buildingdata?UserName={value}");
            ActiveUserName = value;

            await Refresh();
        }

        private string ThresholdFormatter(double value)
        {
            return value.ToString() + "%";
        }

        private string ThresholdParser(string value)
        {
            return value.Replace("%", "");
        }

        private double BuildingConditionThreshold
        {
            get
            {
                return _BuildingConditionThreshold;
            }
            set
            {
                _BuildingConditionThreshold = value;
                if (_BuildingConditionThreshold > 100.0)
                {
                    _BuildingConditionThreshold = 100.0;
                }

                if (_BuildingConditionThreshold < 30.0)
                {
                    _BuildingConditionThreshold = 30.0;
                }

                _ = GlobalAppState.LSSet_Generic<double>("BuildingDataBuildingConditionThreshold-2", _BuildingConditionThreshold);
                ApplyFilters();
            }
        }
        private double _BuildingConditionThreshold = 100.0;

        private List<ShoppingCartModel> ShoppingCart = new List<ShoppingCartModel>();

        private void SelectedBuildingDataChanged()
        {
            ShoppingCart = new List<ShoppingCartModel>();

            var TickerToAmount = new Dictionary<string, int>();

            foreach (var planet in FilteredBuildingData)
            {
                foreach (var building in planet.Children)
                {
                    foreach (var mat in building.Children)
                    {
                        if (mat.CheckState == CheckedState.Checked)
                        {
                            if (!String.IsNullOrWhiteSpace(mat.MaterialTicker) && mat.MaterialAmount != null)
                            {
                                if (!TickerToAmount.ContainsKey(mat.MaterialTicker))
                                {
                                    TickerToAmount.Add(mat.MaterialTicker, (int)mat.MaterialAmount);
                                }
                                else
                                {
                                    TickerToAmount[mat.MaterialTicker] += (int)mat.MaterialAmount;
                                }
                            }
                        }
                    }
                }
            }

            foreach (var kvp in TickerToAmount)
            {
                var scm = new ShoppingCartModel();
                scm.Ticker = kvp.Key;
                scm.Amount = kvp.Value;
                ShoppingCart.Add(scm);
            }

            StateHasChanged();
        }
    }
}