using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

using MatBlazor;
using Microsoft.JSInterop;

namespace FIOWeb.Pages
{
    public partial class Map
    {
        private List<JsonPayloads.MaterialPayload> AllJsonPlanetMaterials = new List<JsonPayloads.MaterialPayload>();
        private List<PlanetDescription> PlanetDescriptions = new List<PlanetDescription>();
        private UniverseMap.SceneDescription sceneDescription = new UniverseMap.SceneDescription();
        private List<JsonPayloads.RainPlanetResource> rainPlanetResources = null;

        private int PlanetDescriptionsPageSize = 50;

        private bool Loading = true;
        private string LoadingProgress = "";

        private void SetProgress(int Numerator, int Denominator)
        {
            double Percent = (double)Numerator / Denominator;
            LoadingProgress = Percent.ToString("P0");
            StateHasChanged();
        }

        private bool bShouldCache = true;

        private async Task TryStoreCacheItem<T>(string Key, T Item)
        {
            if (bShouldCache)
            {
                try
                {
                    await GlobalAppState.LSSet_Generic(Key, Item);
                }
                catch
                {

                }
            }
        }

        protected override async Task OnInitializedAsync()
        {
            GlobalAppState.OnChange += StateHasChanged;

            string CacheStr = "true";
            NavManager.TryGetQueryString<string>("Cache", out CacheStr);
            if (CacheStr != null && CacheStr.ToUpper() == "FALSE")
            {
                bShouldCache = false;
            }

            NavManager.TryGetQueryString<string>("ResetCache", out CacheStr);
            if (CacheStr != null && CacheStr.ToUpper() == "TRUE")
            {
                await GlobalAppState.LS_Clear("Map_WorldSectorsCache");
                await GlobalAppState.LS_Clear("Map_SystemStarsCache");
                await GlobalAppState.LS_Clear("Map_SystemLinksCache");
                await GlobalAppState.LS_Clear("Map_PlanetResourcesCache");
                await GlobalAppState.LS_Clear("Map_AllPlanetMaterialsCache");

                Toaster.Add("Cache Cleared Successfully", MatToastType.Info, "Cache Cleared");
            }

            SetProgress(0, 9);

            const string WorldSectorsCache = "Map_WorldSectorsCache";
            List<JsonPayloads.WorldSector> worldSectors = null;
            if (await GlobalAppState.LS_ContainsKey(WorldSectorsCache))
            {
                worldSectors = await GlobalAppState.LSGet_Generic<List<JsonPayloads.WorldSector>>(WorldSectorsCache);
            }
            else
            {
                var worldSectorsRequest = new Web.Request(HttpMethod.Get, "/systemstars/worldsectors", await GlobalAppState.GetAuthToken());
                worldSectors = await worldSectorsRequest.GetResponseAsync<List<JsonPayloads.WorldSector>>();
                await TryStoreCacheItem(WorldSectorsCache, worldSectors);
            }
            SetProgress(1, 9);

            const string SystemStarsCache = "Map_SystemStarsCache";
            List<JsonPayloads.SystemStarPayload> allStars = null;
            if (await GlobalAppState.LS_ContainsKey(SystemStarsCache))
            {
                allStars = await GlobalAppState.LSGet_Generic<List<JsonPayloads.SystemStarPayload>>(SystemStarsCache);
            }
            else
            {
                var allStarsRequest = new Web.Request(HttpMethod.Get, "/systemstars", await GlobalAppState.GetAuthToken());
                allStars = await allStarsRequest.GetResponseAsync<List<JsonPayloads.SystemStarPayload>>();
                await TryStoreCacheItem(SystemStarsCache, allStars);
            }
            SetProgress(2, 9);

            const string SystemLinksCache = "Map_SystemLinksCache";
            List<JsonPayloads.RainSystemLink> rainSystemLinks = null;
            if (await GlobalAppState.LS_ContainsKey(SystemLinksCache))
            {
                rainSystemLinks = await GlobalAppState.LSGet_Generic<List<JsonPayloads.RainSystemLink>>(SystemLinksCache);
            }
            else
            {
                var rainSystemLinksRequest = new Web.Request(HttpMethod.Get, "/rain/systemlinks", await GlobalAppState.GetAuthToken());
                rainSystemLinks = await rainSystemLinksRequest.GetResponseAsync<List<JsonPayloads.RainSystemLink>>();
                await TryStoreCacheItem(SystemLinksCache, rainSystemLinks);
            }
            SetProgress(3, 9);

            const string PlanetResourcesCache = "Map_PlanetResourcesCache";
            if (await GlobalAppState.LS_ContainsKey(PlanetResourcesCache))
            {
                rainPlanetResources = await GlobalAppState.LSGet_Generic<List<JsonPayloads.RainPlanetResource>>(PlanetResourcesCache);
            }
            else
            {
                var rainPlanetResourcesRequest = new Web.Request(HttpMethod.Get, "/rain/planetresources", await GlobalAppState.GetAuthToken());
                rainPlanetResources = await rainPlanetResourcesRequest.GetResponseAsync<List<JsonPayloads.RainPlanetResource>>();
                await TryStoreCacheItem(PlanetResourcesCache, rainPlanetResources);
            }
            SetProgress(4, 9);

            const string AllPlanetMaterialsCache = "Map_AllPlanetMaterialsCache";
            if (await GlobalAppState.LS_ContainsKey(AllPlanetMaterialsCache))
            {
                AllJsonPlanetMaterials = await GlobalAppState.LSGet_Generic<List<JsonPayloads.MaterialPayload>>(AllPlanetMaterialsCache);
            }
            else
            {
                Web.Request req = null;
                req = new Web.Request(HttpMethod.Get, "/material/category/minerals");
                var minerals = await req.GetResponseAsync<List<JsonPayloads.MaterialPayload>>();
                SetProgress(5, 9);

                req = new Web.Request(HttpMethod.Get, "/material/category/ores");
                var ores = await req.GetResponseAsync<List<JsonPayloads.MaterialPayload>>();
                SetProgress(6, 9);

                req = new Web.Request(HttpMethod.Get, "/material/category/gases");
                var gases = await req.GetResponseAsync<List<JsonPayloads.MaterialPayload>>();
                SetProgress(7, 9);

                req = new Web.Request(HttpMethod.Get, "/material/category/liquids");
                var liquids = await req.GetResponseAsync<List<JsonPayloads.MaterialPayload>>();
                SetProgress(8, 9);

                // Concat all potential resources
                AllJsonPlanetMaterials = minerals;
                AllJsonPlanetMaterials = AllJsonPlanetMaterials.Union(ores).ToList();
                AllJsonPlanetMaterials = AllJsonPlanetMaterials.Union(gases).ToList();
                AllJsonPlanetMaterials = AllJsonPlanetMaterials.Union(liquids).ToList();
                await TryStoreCacheItem(AllPlanetMaterialsCache, AllJsonPlanetMaterials);
            }
            SetProgress(8, 9);


            var allLineSegments = new List<UniverseMap.SubSectorLineSegment>();
            foreach (var worldSector in worldSectors)
            {
                var sector = new UniverseMap.Sector(worldSector);
                allLineSegments.AddRange(sector.GetAllLineSegments());
            }

            var innerLines = allLineSegments.GroupBy(ls => ls).Where(ls => ls.Count() > 1).Select(y => y.Key).ToList();
            var outerLines = allLineSegments.Where(ls => !innerLines.Any(e => (ls == e))).ToList();

            foreach (var innerLine in innerLines)
            {
                innerLine.Color = 0x808080;
                innerLine.Dashed = true;
            }

            sceneDescription.Lines.AddRange(innerLines);
            sceneDescription.Lines.AddRange(outerLines);

            var stars = new List<UniverseMap.SystemStar>();
            foreach (var star in allStars)
            {
                stars.Add(new UniverseMap.SystemStar(star));
            }
            sceneDescription.Stars = stars;

            var links = new List<UniverseMap.FTLConnection>();
            foreach (var rainSystemLink in rainSystemLinks)
            {
                var leftStar = stars.Where(s => s.NaturalId == rainSystemLink.Left).FirstOrDefault();
                var rightStar = stars.Where(s => s.NaturalId == rainSystemLink.Right).FirstOrDefault();

                if (leftStar != null && rightStar != null)
                {
                    var conn = new UniverseMap.FTLConnection();

                    conn.StartSystemId = leftStar.NaturalId;
                    conn.StartPositionX = leftStar.PositionX;
                    conn.StartPositionY = leftStar.PositionY;
                    conn.StartPositionZ = leftStar.PositionZ;
                    conn.EndSystemId = rightStar.NaturalId;
                    conn.EndPositionX = rightStar.PositionX;
                    conn.EndPositionY = rightStar.PositionY;
                    conn.EndPositionZ = rightStar.PositionZ;
                    links.Add(conn);
                }
            }
            sceneDescription.Connections = links;
            Loading = false;
        }

        public void Dispose()
        {
            GlobalAppState.OnChange -= StateHasChanged;
        }

        private bool SceneInitialized = false;
        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (!firstRender)
            {
                if (!SceneInitialized && !Loading)
                {
                    SceneInitialized = true;
                    await JSRuntime.InvokeVoidAsync("ThreeJSFunctions.load", sceneDescription);
                    await SendDotNetInstanceToJS();
                }
            }
        }

        public class PlanetDescription
        {
            public string Planet { get; set; }
            public bool IsTier1 { get; set; }
            public double Fertility { get; set; }
            public string FertilityDisplay
            {
                get
                {
                    if (Fertility == -1.0)
                    {
                        return "--";
                    }
                    else
                    {
                        return Fertility.ToString("P2");
                    }
                }
            }
            public string Resource { get; set; }
            public string Type { get; set; }
            public double DailyExtraction { get; set; }
            public string DailyExractionDisplay
            {
                get
                {
                    return DailyExtraction.ToString("N2");
                }
            }
        }

        private Dictionary<string, List<PlanetDescription>> SystemToPlanetDescriptionsCache = new Dictionary<string, List<PlanetDescription>>();

        [JSInvokable]
        public async Task SetSystemNaturalId(string systemId)
        {
            if (SystemToPlanetDescriptionsCache.ContainsKey(systemId))
            {
                PlanetDescriptions = SystemToPlanetDescriptionsCache[systemId];
                StateHasChanged();
                return;
            }

            PlanetDescriptions = null;
            StateHasChanged();

            // Get all the planets we need to query:
            var planets = rainPlanetResources.Where(rpr => rpr.Planet.ToUpper().StartsWith(systemId.ToUpper())).Select(p => p.Planet).Distinct().ToList();

            var allPlanetData = new List<JsonPayloads.PlanetDataPayload>();
            foreach (var planet in planets)
            {
                var planetRequest = new Web.Request(HttpMethod.Get, $"/planet/{planet}");
                allPlanetData.Add(await planetRequest.GetResponseAsync<JsonPayloads.PlanetDataPayload>());
            }

            var workingPlanetDescs = new List<PlanetDescription>();
            foreach (var planetData in allPlanetData)
            {
                var PlanetDisplayName = (planetData.PlanetName != planetData.PlanetNaturalId) ? $"{planetData.PlanetName} ({planetData.PlanetNaturalId})" : planetData.PlanetNaturalId;
                var PlanetIsTier1 = planetData.Surface && (planetData.Gravity >= 0.25 && planetData.Gravity <= 2.5) && (planetData.Pressure >= 0.25 && planetData.Pressure <= 2.0) && (planetData.Temperature >= -25.0 && planetData.Temperature <= 75.0);
                var PlanetFertility = planetData.Fertility;

                foreach (var resource in planetData.Resources)
                {
                    var desc = new PlanetDescription();
                    desc.Planet = PlanetDisplayName;
                    desc.IsTier1 = PlanetIsTier1;
                    desc.Fertility = PlanetFertility;
                    desc.Resource = AllJsonPlanetMaterials.Where(m => m.MatId == resource.MaterialId).Select(m => m.Ticker).First();
                    desc.Type = resource.ResourceType;
                    desc.DailyExtraction = (desc.Type == "GASEOUS") ? resource.Factor * 60.0 : resource.Factor * 70.0;
                    workingPlanetDescs.Add(desc);
                }
            }
            PlanetDescriptions = workingPlanetDescs.OrderBy(p => p.Planet).ToList();
            SystemToPlanetDescriptionsCache.Add(systemId, PlanetDescriptions);
            StateHasChanged();
        }

        private async Task SendDotNetInstanceToJS()
        {
            var dotNetObjRef = DotNetObjectReference.Create(this);
            await JSRuntime.InvokeVoidAsync("ThreeJSFunctions.setMapHandler", dotNetObjRef);
        }
    }
}