using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using FIOWeb.Models;
using FIOWeb.JsonPayloads;

using AntDesign;
using MatBlazor;

namespace FIOWeb.Pages
{
    public partial class PlanetSearch
    {
        private MatTheme blackTheme = new MatTheme()
        {
            Primary = "black",
            Secondary = "black"
        };

        private bool PageLoading = true;

        private bool DrawerVisible = false;

        private List<PlanetSearchModel> PlanetSearchModels = new List<PlanetSearchModel>();
        private List<PlanetResource> PlanetResources = new List<PlanetResource>(Enumerable.Range(0, 4).Select(x => new PlanetResource()));

        private List<bool> AllPlanetMaterialsChecked = new List<bool>();
        private List<string> AllPlanetMaterials = new List<string>();

        private List<string> ExchangeLocationNames = new List<string>();

        private List<MaterialPayload> AllJsonPlanetMaterials = new List<MaterialPayload>();

        bool SearchLoading = false;

        class PlanetSearchState
        {
            // Materials
            public IEnumerable<string> SelectedMaterials { get; set; } = new List<string>();

            // Must have settings
            public bool Fertile { get; set; } = false;
            public bool LM { get; set; } = false;
            public bool COGC { get; set; } = false;
            public bool WAR { get; set; } = false;
            public bool ADM { get; set; } = false;
            public bool SHY { get; set; } = false;

            // Can have settings
            public bool Rocky { get; set; } = true;
            public bool Gaseous { get; set; } = false;
            public bool LowGravity { get; set; } = false;
            public bool HighGravity { get; set; } = false;
            public bool LowPressure { get; set; } = false;
            public bool HighPressure { get; set; } = false;
            public bool LowTemperature { get; set; } = false;
            public bool HighTemperature { get; set; } = false;

            // Visibility
            public bool ShowFertility { get; set; } = true;
            public bool ShowSurfaceType { get; set; } = true;
            public bool ShowGravity { get; set; } = true;
            public bool ShowPressure { get; set; } = true;
            public bool ShowTemperature { get; set; } = true;
            public bool ShowLM { get; set; } = true;
            public bool ShowWAR { get; set; } = true;
            public bool ShowADM { get; set; } = true;
            public bool ShowSHY { get; set; } = true;
            public bool ShowCOGC { get; set; } = true;

            public List<bool> ShowExchangeJumps { get; set; } = new List<bool>();

            public bool ShowCurrency { get; set; } = false;
            public bool ShowTotalPioneers { get; set; } = false;
            public bool ShowUnemployedPioneers { get; set; } = false;
            public bool ShowTotalSettlers { get; set; } = false;
            public bool ShowUnemployedSettlers { get; set; } = false;
            public bool ShowTotalTechnicians { get; set; } = false;
            public bool ShowUnemployedTechnicians { get; set; } = false;
            public bool ShowTotalEngineers { get; set; } = false;
            public bool ShowUnemployedEngineers { get; set; } = false;
            public bool ShowTotalScientists { get; set; } = false;
            public bool ShowUnemployedScientists { get; set; } = false;
            public bool ShowTotalPlots { get; set; } = false;

            public void ClearMustHaveSettings()
            {
                Fertile = false;
                LM = false;
                COGC = false;
                WAR = false;
                ADM = false;
                SHY = false;
            }

            public void ClearCanHaveSettings()
            {
                Rocky = false;
                Gaseous = false;
                LowGravity = false;
                HighGravity = false;
                LowPressure = false;
                HighPressure = false;
                LowTemperature = false;
                HighTemperature = false;
            }
        }

        private int PlanetSearchPageSize = 50;

        PlanetSearchState state = new PlanetSearchState();

        private MatSortChangedEvent LastPlanetSearchModelSortEvent = null;
        private void SortPlanetSearchModels(MatSortChangedEvent sort)
        {
            LastPlanetSearchModelSortEvent = sort;
            if (sort != null && sort.Direction != MatSortDirection.None && !String.IsNullOrWhiteSpace(sort.SortId))
            {
                bool bAscending = (sort.Direction == MatSortDirection.Asc);
                switch (sort.SortId)
                {
                    case "planet":
                        PlanetSearchModels = bAscending ? PlanetSearchModels.OrderBy(psm => psm.PlanetNaturalId).ToList() : PlanetSearchModels.OrderByDescending(psm => psm.PlanetNaturalId).ToList();
                        break;
                    case "materials":
                        PlanetSearchModels = bAscending ? PlanetSearchModels.OrderBy(psm => psm.MaterialSummary).ToList() : PlanetSearchModels.OrderByDescending(psm => psm.MaterialSummary).ToList();
                        break;
                    case "material0":
                        string material0 = state.SelectedMaterials.ToList()[0];
                        PlanetSearchModels = bAscending ? PlanetSearchModels.OrderBy(psm => psm.Resources.Where(r => r.Material == material0).Min(r => r.DailyExtraction)).ToList() : PlanetSearchModels.OrderByDescending(psm => psm.Resources.Where(r => r.Material == material0).Min(r => r.DailyExtraction)).ToList();
                        break;
                    case "material1":
                        string material1 = state.SelectedMaterials.ToList()[1];
                        PlanetSearchModels = bAscending ? PlanetSearchModels.OrderBy(psm => psm.Resources.Where(r => r.Material == material1).Min(r => r.DailyExtraction)).ToList() : PlanetSearchModels.OrderByDescending(psm => psm.Resources.Where(r => r.Material == material1).Min(r => r.DailyExtraction)).ToList();
                        break;
                    case "material2":
                        string material2 = state.SelectedMaterials.ToList()[2];
                        PlanetSearchModels = bAscending ? PlanetSearchModels.OrderBy(psm => psm.Resources.Where(r => r.Material == material2).Min(r => r.DailyExtraction)).ToList() : PlanetSearchModels.OrderByDescending(psm => psm.Resources.Where(r => r.Material == material2).Min(r => r.DailyExtraction)).ToList();
                        break;
                    case "material3":
                        string material3 = state.SelectedMaterials.ToList()[3];
                        PlanetSearchModels = bAscending ? PlanetSearchModels.OrderBy(psm => psm.Resources.Where(r => r.Material == material3).Min(r => r.DailyExtraction)).ToList() : PlanetSearchModels.OrderByDescending(psm => psm.Resources.Where(r => r.Material == material3).Min(r => r.DailyExtraction)).ToList();
                        break;
                    case "fertility":
                        PlanetSearchModels = bAscending ? PlanetSearchModels.OrderBy(psm => psm.Fertility).ToList() : PlanetSearchModels.OrderByDescending(psm => psm.Fertility).ToList();
                        break;
                    case "surfacetype":
                        PlanetSearchModels = bAscending ? PlanetSearchModels.OrderBy(psm => psm.SurfaceType).ToList() : PlanetSearchModels.OrderByDescending(psm => psm.SurfaceType).ToList();
                        break;
                    case "gravity":
                        PlanetSearchModels = bAscending ? PlanetSearchModels.OrderBy(psm => psm.Gravity).ToList() : PlanetSearchModels.OrderByDescending(psm => psm.Gravity).ToList();
                        break;
                    case "pressure":
                        PlanetSearchModels = bAscending ? PlanetSearchModels.OrderBy(psm => psm.Pressure).ToList() : PlanetSearchModels.OrderByDescending(psm => psm.Pressure).ToList();
                        break;
                    case "temperature":
                        PlanetSearchModels = bAscending ? PlanetSearchModels.OrderBy(psm => psm.Temperature).ToList() : PlanetSearchModels.OrderByDescending(psm => psm.Temperature).ToList();
                        break;
                    case "lm":
                        PlanetSearchModels = bAscending ? PlanetSearchModels.OrderBy(psm => psm.HasLocalMarket).ToList() : PlanetSearchModels.OrderByDescending(psm => psm.HasLocalMarket).ToList();
                        break;
                    case "war":
                        PlanetSearchModels = bAscending ? PlanetSearchModels.OrderBy(psm => psm.HasWarehouse).ToList() : PlanetSearchModels.OrderByDescending(psm => psm.HasWarehouse).ToList();
                        break;
                    case "adm":
                        PlanetSearchModels = bAscending ? PlanetSearchModels.OrderBy(psm => psm.HasAdministrationCenter).ToList() : PlanetSearchModels.OrderByDescending(psm => psm.HasAdministrationCenter).ToList();
                        break;
                    case "shy":
                        PlanetSearchModels = bAscending ? PlanetSearchModels.OrderBy(psm => psm.HasShipyard).ToList() : PlanetSearchModels.OrderByDescending(psm => psm.HasShipyard).ToList();
                        break;
                    case "cogc":
                        PlanetSearchModels = bAscending ? PlanetSearchModels.OrderBy(psm => psm.ActiveCOGCDisplay).ToList() : PlanetSearchModels.OrderByDescending(psm => psm.ActiveCOGCDisplay).ToList();
                        break;
                    case "currency":
                        PlanetSearchModels = bAscending ? PlanetSearchModels.OrderBy(psm => psm.CurrencyCode).ToList() : PlanetSearchModels.OrderByDescending(psm => psm.CurrencyCode).ToList();
                        break;
                    case "totalpioneers":
                        PlanetSearchModels = bAscending ? PlanetSearchModels.OrderBy(psm => psm.TotalPioneers).ToList() : PlanetSearchModels.OrderByDescending(psm => psm.TotalPioneers).ToList();
                        break;
                    case "unemployedpioneers":
                        PlanetSearchModels = bAscending ? PlanetSearchModels.OrderBy(psm => psm.OpenPioneers).ToList() : PlanetSearchModels.OrderByDescending(psm => psm.OpenPioneers).ToList();
                        break;
                    case "totalsettlers":
                        PlanetSearchModels = bAscending ? PlanetSearchModels.OrderBy(psm => psm.TotalSettlers).ToList() : PlanetSearchModels.OrderByDescending(psm => psm.TotalSettlers).ToList();
                        break;
                    case "unemployedsettlers":
                        PlanetSearchModels = bAscending ? PlanetSearchModels.OrderBy(psm => psm.OpenSettlers).ToList() : PlanetSearchModels.OrderByDescending(psm => psm.OpenSettlers).ToList();
                        break;
                    case "totaltechnicians":
                        PlanetSearchModels = bAscending ? PlanetSearchModels.OrderBy(psm => psm.TotalTechnicians).ToList() : PlanetSearchModels.OrderByDescending(psm => psm.TotalTechnicians).ToList();
                        break;
                    case "unemployedtechnicians":
                        PlanetSearchModels = bAscending ? PlanetSearchModels.OrderBy(psm => psm.OpenTechnicians).ToList() : PlanetSearchModels.OrderByDescending(psm => psm.OpenTechnicians).ToList();
                        break;
                    case "totalengineers":
                        PlanetSearchModels = bAscending ? PlanetSearchModels.OrderBy(psm => psm.TotalEngineers).ToList() : PlanetSearchModels.OrderByDescending(psm => psm.TotalEngineers).ToList();
                        break;
                    case "unemployedengineers":
                        PlanetSearchModels = bAscending ? PlanetSearchModels.OrderBy(psm => psm.OpenEngineers).ToList() : PlanetSearchModels.OrderByDescending(psm => psm.OpenEngineers).ToList();
                        break;
                    case "totalscientists":
                        PlanetSearchModels = bAscending ? PlanetSearchModels.OrderBy(psm => psm.TotalScientists).ToList() : PlanetSearchModels.OrderByDescending(psm => psm.TotalScientists).ToList();
                        break;
                    case "unemployedscientists":
                        PlanetSearchModels = bAscending ? PlanetSearchModels.OrderBy(psm => psm.OpenScientists).ToList() : PlanetSearchModels.OrderByDescending(psm => psm.OpenScientists).ToList();
                        break;
                    case "totalplots":
                        PlanetSearchModels = bAscending ? PlanetSearchModels.OrderBy(psm => psm.TotalPlots).ToList() : PlanetSearchModels.OrderByDescending(psm => psm.TotalPlots).ToList();
                        break;
                }

                if (sort.SortId.EndsWith("_jumps"))
                {
                    int idx = int.Parse(sort.SortId.Replace("_jumps", ""));
                    PlanetSearchModels = bAscending ? PlanetSearchModels.OrderBy(psm => psm.DistanceResults[idx]).ToList() : PlanetSearchModels.OrderByDescending(psm => psm.DistanceResults[idx]).ToList();
                }
            }
        }

        private MatSortChangedEvent LastPlanetResourcesSortEvent = null;
        private void SortPlanetResources(MatSortChangedEvent sort)
        {
            LastPlanetResourcesSortEvent = sort;

            // First pull out filler entries
            PlanetResources.RemoveAll(pr => pr.IsValid == false);

            if (sort != null && sort.Direction != MatSortDirection.None && !String.IsNullOrWhiteSpace(sort.SortId))
            {
                switch (sort.SortId)
                {
                    case "material":
                        PlanetResources = (sort.Direction == MatSortDirection.Asc) ? PlanetResources.OrderBy(pr => pr.Material).ToList() : PlanetResources.OrderByDescending(pr => pr.Material).ToList();
                        break;
                    case "resourcetype":
                        PlanetResources = (sort.Direction == MatSortDirection.Asc) ? PlanetResources.OrderBy(pr => pr.ResourceType).ToList() : PlanetResources.OrderByDescending(pr => pr.ResourceType).ToList();
                        break;
                    case "concentration":
                        PlanetResources = (sort.Direction == MatSortDirection.Asc) ? PlanetResources.OrderBy(pr => pr.Concentration).ToList() : PlanetResources.OrderByDescending(pr => pr.Concentration).ToList();
                        break;
                    case "dailyextraction":
                        PlanetResources = (sort.Direction == MatSortDirection.Asc) ? PlanetResources.OrderBy(pr => pr.DailyExtraction).ToList() : PlanetResources.OrderByDescending(pr => pr.DailyExtraction).ToList();
                        break;
                }
            }

            // Add in filler entries
            int NumResourcesToAdd = 4 - PlanetResources.Count;
            if (NumResourcesToAdd > 0)
            {
                PlanetResources.AddRange(Enumerable.Range(0, NumResourcesToAdd).Select(x => new PlanetResource()));
            }
        }

        private void PlanetSearchModelsSelectionChangedEvent(object row)
        {
            var psm = row as PlanetSearchModel;
            if (psm != null)
            {
                PlanetResources = psm.Resources.OrderByDescending(r => r.DailyExtraction).ToList();
            }
            else
            {
                PlanetResources = new List<PlanetResource>();
                PlanetResources.AddRange(Enumerable.Range(0, 4).Select(x => new PlanetResource()));
            }

            int NumResourcesToAdd = 4 - PlanetResources.Count;
            if (NumResourcesToAdd > 0)
            {
                PlanetResources.AddRange(Enumerable.Range(0, NumResourcesToAdd).Select(x => new PlanetResource()));
            }

            SortPlanetResources(LastPlanetResourcesSortEvent);
            StateHasChanged();
        }

        private void FillSearchStateFromUri(ref PlanetSearchState state)
        {
            // Clear the Uri settings
            state.SelectedMaterials = new List<string>();
            state.ClearMustHaveSettings();
            state.ClearCanHaveSettings();

            var uriQuery = NavManager.ToAbsoluteUri(NavManager.Uri).Query;

            foreach (var majorToken in uriQuery.Split(new char[] { '&' }, StringSplitOptions.RemoveEmptyEntries))
            {
                var subTokens = majorToken.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                if (subTokens.Length == 2)
                {
                    var key = subTokens[0].ToUpper().TrimStart('?');
                    var value = subTokens[1].ToUpper();
                    switch (key)
                    {
                        case "RESOURCES":
                            state.SelectedMaterials = value.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Take(4).ToList();
                            break;
                        case "FERTILE":
                            state.Fertile = (value == "TRUE");
                            break;
                        case "LM":
                            state.LM = (value == "TRUE");
                            break;
                        case "COGC":
                            state.COGC = (value == "TRUE");
                            break;
                        case "WAR":
                            state.WAR = (value == "TRUE");
                            break;
                        case "ADM":
                            state.ADM = (value == "TRUE");
                            break;
                        case "SHY":
                            state.SHY = (value == "TRUE");
                            break;
                        case "ROCKY":
                            state.Rocky = (value == "TRUE");
                            break;
                        case "GAS":
                            state.Gaseous = (value == "TRUE");
                            break;
                        case "LOWGRAV":
                            state.LowGravity = (value == "TRUE");
                            break;
                        case "HIGHGRAV":
                            state.HighGravity = (value == "TRUE");
                            break;
                        case "LOWPRES":
                            state.LowPressure = (value == "TRUE");
                            break;
                        case "HIGHPRES":
                            state.HighPressure = (value == "TRUE");
                            break;
                        case "LOWTEMP":
                            state.LowTemperature = (value == "TRUE");
                            break;
                        case "HIGHTEMP":
                            state.HighTemperature = (value == "TRUE");
                            break;
                    }
                }
            }
        }

        private string GetUriFromSearchPayload(PlanetSearchPayload search)
        {
            StringBuilder sb = new StringBuilder();
            if (search.Materials.Count > 0)
            {
                sb.Append("Resources=");
                sb.Append(String.Join(';', search.Materials));
                sb.Append("&");
            }

            if (search.MustBeFertile)
            {
                sb.Append("Fertile=true&");
            }
            if (search.MustHaveLocalMarket)
            {
                sb.Append("LM=true&");
            }
            if (search.MustHaveChamberOfCommerce)
            {
                sb.Append("COGC=true&");
            }
            if (search.MustHaveWarehouse)
            {
                sb.Append("WAR=true&");
            }
            if (search.MustHaveAdministrationCenter)
            {
                sb.Append("ADM=true&");
            }
            if (search.MustHaveShipyard)
            {
                sb.Append("SHY=true&");
            }

            if (search.IncludeRocky)
            {
                sb.Append("Rocky=true&");
            }
            if (search.IncludeGaseous)
            {
                sb.Append("Gas=true&");
            }
            if (search.IncludeLowGravity)
            {
                sb.Append("LowGrav=true&");
            }
            if (search.IncludeHighGravity)
            {
                sb.Append("HighGrav=true&");
            }
            if (search.IncludeLowPressure)
            {
                sb.Append("LowPres=true&");
            }
            if (search.IncludeHighPressure)
            {
                sb.Append("HighPres=true&");
            }
            if (search.IncludeLowTemperature)
            {
                sb.Append("LowTemp=true&");
            }
            if (search.IncludeHighTemperature)
            {
                sb.Append("HighTemp=true&");
            }

            return sb.ToString().TrimEnd('&');
        }

        private async Task SearchClick()
        {
            if (!state.Rocky && !state.Gaseous)
            {
                modalService.Error(new ConfirmOptions()
                {
                    Title = "Missing Surface Type",
                    Content = "Requires one or both of 'Rocky' and 'Gaseous' to be checked."
                });
                return;
            }

            SearchLoading = true;

            PlanetResources.Clear();
            PlanetSearchModels.Clear();

            var search = new PlanetSearchPayload();
            search.Materials.AddRange(state.SelectedMaterials);
            search.ConcentrationThreshold = ConcentrationThreshold / 100.0;

            search.MustBeFertile = state.Fertile;
            search.MustHaveLocalMarket = state.LM;
            search.MustHaveChamberOfCommerce = state.COGC;
            search.MustHaveWarehouse = state.WAR;
            search.MustHaveAdministrationCenter = state.ADM;
            search.MustHaveShipyard = state.SHY;

            search.IncludeRocky = state.Rocky;
            search.IncludeGaseous = state.Gaseous;
            search.IncludeLowGravity = state.LowGravity;
            search.IncludeHighGravity = state.HighGravity;
            search.IncludeLowPressure = state.LowPressure;
            search.IncludeHighPressure = state.HighPressure;
            search.IncludeLowTemperature = state.LowTemperature;
            search.IncludeHighTemperature = state.HighTemperature;

            search.DistanceChecks.AddRange(ExchangeLocationNames);

            string uri = GetUriFromSearchPayload(search);
            NavManager.NavigateTo($"/planetsearch?{uri}");

            PlanetSearchModels = await PlanetSearchModel.Search(AllJsonPlanetMaterials, search);
            SortPlanetSearchModels(LastPlanetSearchModelSortEvent);

            SearchLoading = false;

            _ = GlobalAppState.LSSet_Generic<PlanetSearchState>("PlanetSearchState-4", state);
        }

        private string GetHexForColor(System.Drawing.Color c)
        {
            return "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
        }

        private string GetConcentrationColor(PlanetResource model)
        {
            double alpha = model.Concentration;
            int component = (int)(255.0 * alpha);
            int background = (int)(255.0 * (1.0 - alpha));

            System.Drawing.Color c = System.Drawing.Color.FromArgb(background, component + background, background);
            string colorStr = GetHexForColor(c);
            return colorStr;
        }

        private string GetDailyExtractionColor(PlanetResource model)
        {
            double alpha = model.Concentration;
            int component = (int)(255.0 * alpha);
            int background = (int)(255.0 * (1.0 - alpha));

            System.Drawing.Color c = System.Drawing.Color.FromArgb(component + background, component + background, background);
            return GetHexForColor(c);
        }

        protected override async Task OnInitializedAsync()
        {
            state = await GlobalAppState.LSGet_Generic<PlanetSearchState>("PlanetSearchState-3");
            if (state == null)
            {
                state = new PlanetSearchState();
            }

            var uriQuery = NavManager.ToAbsoluteUri(NavManager.Uri).Query;
            if (uriQuery.Length > 3)
            {
                FillSearchStateFromUri(ref state);
            }

            Web.Request req = null;

            req = new Web.Request(HttpMethod.Get, "/material/category/minerals");
            var minerals = await req.GetResponseAsync<List<MaterialPayload>>();

            req = new Web.Request(HttpMethod.Get, "/material/category/ores");
            var ores = await req.GetResponseAsync<List<MaterialPayload>>();

            req = new Web.Request(HttpMethod.Get, "/material/category/gases");
            var gases = await req.GetResponseAsync<List<MaterialPayload>>();

            req = new Web.Request(HttpMethod.Get, "/material/category/liquids");
            var liquids = await req.GetResponseAsync<List<MaterialPayload>>();

            // Concat all potential resources
            AllJsonPlanetMaterials = minerals;
            AllJsonPlanetMaterials = AllJsonPlanetMaterials.Union(ores).ToList();
            AllJsonPlanetMaterials = AllJsonPlanetMaterials.Union(gases).ToList();
            AllJsonPlanetMaterials = AllJsonPlanetMaterials.Union(liquids).ToList();

            // Extract just the ticker and sort
            AllPlanetMaterials = AllJsonPlanetMaterials.OrderBy(r => r.Ticker).Select(r => r.Ticker).ToList();
            AllPlanetMaterialsChecked = new List<bool>(new bool[AllPlanetMaterials.Count]);
            foreach (var stateMat in state.SelectedMaterials)
            {
                int idx = AllPlanetMaterials.IndexOf(stateMat);
                if (idx >= 0)
                {
                    AllPlanetMaterialsChecked[idx] = true;
                }
            }

            // Grab exchanges
            req = new Web.Request(HttpMethod.Get, "/global/comexexchanges");
            var exchanges = await req.GetResponseAsync<List<ExchangePayload>>();

            ExchangeLocationNames = exchanges.Select(e => e.LocationName).ToList();
            if (state.ShowExchangeJumps.Count != ExchangeLocationNames.Count)
            {
                state.ShowExchangeJumps = Enumerable.Repeat(true, ExchangeLocationNames.Count).ToList();
            }

            StateHasChanged();
            PageLoading = false;
        }

        private async Task CopyToClipboard(string text)
        {
            await Clipboard.WriteTextAsync(text);
            Toaster.Add("Copied to COGC buffer command to clipboard.", MatToastType.Info, "Copied");
        }

        private double ConcentrationThreshold = 0.0;

        private string ConcentrationThresholdFormatter(double value)
        {
            return value.ToString() + "%";
        }

        private string ConcentrationThresholdParser(string value)
        {
            return value.Replace("%", "");
        }

        private double ProductionEfficiencyPercentage
        {
            get
            {
                return ProductionEfficiency / 100.0;
            }
        }

        private double ProductionEfficiency = 100.0;

        private string ProductionEfficiencyFormatter(double value)
        {
            return value.ToString() + "%";
        }

        private string ProductionEfficiencyParser(string value)
        {
            return value.Replace("%", "");
        }
    }
}