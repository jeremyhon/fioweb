using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

using AntDesign;
using MatBlazor;
using Newtonsoft.Json;

using FIOWeb.JsonPayloads;


namespace FIOWeb.Pages
{
    public partial class Settings
    {
        private string CurrentUser = null;
        private List<string> AllUsers = null;
        private List<PermissionPayload> Permissions = null;
        private List<JsonAuthAPIKeyPayload> APIKeys = null;
        private List<GroupPayload> Groups = null;
        private DiscordIdPayload DiscIdPayload = null;

        protected override async Task OnInitializedAsync()
        {
            GlobalAppState.OnChange += StateHasChanged;

            CurrentUser = await GlobalAppState.GetUserName();

            var allUsersRequest = new Web.Request(HttpMethod.Get, "/user/allusers", await GlobalAppState.GetAuthToken());
            AllUsers = await allUsersRequest.GetResponseAsync<List<string>>();
            AllUsers.Sort();

            var permissionRequest = new Web.Request(HttpMethod.Get, "/auth/permissions", await GlobalAppState.GetAuthToken());
            Permissions = await permissionRequest.GetResponseAsync<List<PermissionPayload>>();

            var discordIdRequest = new Web.Request(HttpMethod.Get, $"/auth/discordid/{CurrentUser}", await GlobalAppState.GetAuthToken());
            DiscIdPayload = await discordIdRequest.GetResponseAsync<DiscordIdPayload>();
            DiscordId = DiscIdPayload.DiscordId;

            await RefreshAPIKeys();
            await RefreshGroups();
        }

        private async Task RefreshAPIKeys()
        {
            var apiKeysRequest = new Web.Request(HttpMethod.Get, "/auth/listapikeys", await GlobalAppState.GetAuthToken());
            APIKeys = await apiKeysRequest.GetResponseAsync<List<JsonAuthAPIKeyPayload>>();
        }

        private async Task RefreshGroups()
		{
            var groupsRequest = new Web.Request(HttpMethod.Get, "/auth/groups", await GlobalAppState.GetAuthToken());
            Groups = await groupsRequest.GetResponseAsync<List<GroupPayload>>();

        }

        public void Dispose()
        {
            GlobalAppState.OnChange -= StateHasChanged;
        }


        #region Reset All Data
        private bool ResetAllDataDlgVisible = false;
        private async Task ResetAllDataOnOk()
        {
            var resetDataRequest = new Web.Request(HttpMethod.Post, "/user/resetalldata", await GlobalAppState.GetAuthToken());
            _ = resetDataRequest.GetResultNoResponse();

            Toaster.Add("All data has been reset", MatToastType.Info, "Data Reset");
            ResetAllDataDlgVisible = false;
            await Task.Delay(500);
        }

        private async Task ResetAllDataOnCancel()
        {
            ResetAllDataDlgVisible = false;
            await Task.Delay(500);
        }
        #endregion

        #region Set Discord Id
        private string DiscordId = "";
        private bool SetDiscordIdDlgVisible = false;
        private async Task SetDiscordIdOnOk()
		{
            var changeDiscordIdPayload = new DiscordIdPayload();
            changeDiscordIdPayload.DiscordId = DiscordId;
            var setDiscordIdRequest = new Web.Request(HttpMethod.Post, "/auth/discordid", await GlobalAppState.GetAuthToken(), JsonConvert.SerializeObject(changeDiscordIdPayload));
            await setDiscordIdRequest.GetResultNoResponse();

            SetDiscordIdDlgVisible = false;
            await Task.Delay(500);

            if (setDiscordIdRequest.StatusCode == HttpStatusCode.OK)
			{
                Toaster.Add("Changed DiscordId Successfully.", MatToastType.Success, "Success");
			}
            else
			{
                Toaster.Add("Failed to change DiscordId", MatToastType.Danger, "Change DiscordId Failed");
			}
		}

        private async Task SetDiscordIdOnCancel()
		{
            SetDiscordIdDlgVisible = false;
            await Task.Delay(500);
        }
        #endregion

        #region Change Password
        private string OldPassword = "";
        private string NewPassword = "";
        private string NewPassword2 = "";
        private bool ChangePasswordDlgVisible = false;
        private async Task ChangePasswordOnOk()
        {
            if (NewPassword == NewPassword2)
            {
                var changePasswordPayload = new JsonChangePasswordPayload();
                changePasswordPayload.OldPassword = OldPassword;
                changePasswordPayload.NewPassword = NewPassword;
                var changePasswordRequest = new Web.Request(HttpMethod.Post, "/auth/changepassword", await GlobalAppState.GetAuthToken(), JsonConvert.SerializeObject(changePasswordPayload));
                await changePasswordRequest.GetResultNoResponse();

                ChangePasswordDlgVisible = false;
                await Task.Delay(500);

                if (changePasswordRequest.StatusCode == HttpStatusCode.OK)
                {
                    Toaster.Add("Password changed successfully.", MatToastType.Success, "Success");
                }
                else
                {
                    Toaster.Add("Failed to authenticate with previous password", MatToastType.Danger, "Password Change Failed");
                }
            }
            else
            {
                ChangePasswordDlgVisible = false;
                await Task.Delay(500);

                Toaster.Add("New passwords did not match", MatToastType.Danger, "Password Change Failed");
            }
        }

        private async Task ChangePasswordOnCancel()
        {
            ChangePasswordDlgVisible = false;
            await Task.Delay(500);
        }
        #endregion

        #region API Key
        private ITable APIKeyTable;

        private string APIKeyApplication = null;
        private string APIKeyGuid = null;
        private string APIKeyPassword = null;
        private bool APIKeyAllowWrites = false;

        #region Create API Key
        private bool CreateAPIKeyDialogVisible = false;

        private async Task CreateAPIKeyOnOk()
        {
            JsonAuthCreateAPIKeyPayload createPayload = new JsonAuthCreateAPIKeyPayload
            {
                UserName = await GlobalAppState.GetUserName(),
                Password = APIKeyPassword,
                AllowWrites = APIKeyAllowWrites,
                Application = APIKeyApplication
            };
            var createAPIKeyRequest = new Web.Request(HttpMethod.Post, "/auth/createapikey", await GlobalAppState.GetAuthToken(), JsonConvert.SerializeObject(createPayload));
            await createAPIKeyRequest.GetResultNoResponse();
            if (createAPIKeyRequest.StatusCode == HttpStatusCode.OK)
            {
                Toaster.Add("API Key Created", MatToastType.Success, "Success");
                await RefreshAPIKeys();
                StateHasChanged();
            }
            else if (createAPIKeyRequest.StatusCode == HttpStatusCode.Unauthorized)
            {
                Toaster.Add("Failed to authenticate", MatToastType.Danger, "Login failure");
            }
            else if (createAPIKeyRequest.StatusCode == HttpStatusCode.NotAcceptable)
            {
                Toaster.Add("Too many API keys", MatToastType.Danger, "Limit 20");
            }
            else
            {
                Toaster.Add("Unknown error occurred", MatToastType.Danger, "???");
            }

            CreateAPIKeyDialogVisible = false;
            await Task.Delay(500);
        }

        private async Task CreateAPIKeyOnCancel()
        {
            CreateAPIKeyDialogVisible = false;
            await Task.Delay(500);
        }

        void ToggleAPIKeyAllowWritesChecked(bool value)
        {
            APIKeyAllowWrites = value;
        }
        #endregion

        #region Delete API Key
        private bool DeleteAPIKeyDialogVisible = false;
        private async Task DeleteAPIKeyOnOk()
        {
            var deletePayload = new JsonAuthDeleteAPIKeyPayload
            {
                UserName = await GlobalAppState.GetUserName(),
                Password = APIKeyPassword,
                ApiKeyToRevoke = APIKeyGuid
            };

            var deleteApiKey = new Web.Request(HttpMethod.Post, "/auth/revokeapikey", await GlobalAppState.GetAuthToken(), JsonConvert.SerializeObject(deletePayload));
            await deleteApiKey.GetResultNoResponse();
            if (deleteApiKey.StatusCode == HttpStatusCode.OK)
            {
                Toaster.Add("API Key Deleted", MatToastType.Success, "Success");
                APIKeys.RemoveAll(ak => ak.AuthAPIKey == APIKeyGuid);
                StateHasChanged();
            }
            else if (deleteApiKey.StatusCode == HttpStatusCode.Unauthorized)
            {
                Toaster.Add("Failed to authenticate", MatToastType.Danger, "Login failure");
            }
            else
            {
                Toaster.Add("Unknown error occurred", MatToastType.Danger, "???");
            }

            DeleteAPIKeyDialogVisible = false;
            await Task.Delay(500);
        }

        private async Task DeleteAPIKeyOnCancel()
        {
            DeleteAPIKeyDialogVisible = false;
            await Task.Delay(500);
        }
        #endregion
        #endregion

        #region Permissions

        private ITable PermissionTable;

        private bool PermissionUserDrawerVisible = false;
        private IEnumerable<string> PermissionSelectedUsers;

        private List<string> PermissionRemainingUsers
        {
            get
            {
                if (_PermissionRemainingUsers == null)
                {
                    List<string> permissionUsers = Permissions.Select(p => p.UserName).ToList();
                    List<string> allUsers = new List<string>(AllUsers);
                    allUsers.Add("*");
                    allUsers.Remove(CurrentUser);

                    var listToShow = allUsers.Except(permissionUsers).Union(permissionUsers.Except(allUsers)).ToList();
                    listToShow.Sort();
                    _PermissionRemainingUsers = listToShow;
                }

                return _PermissionRemainingUsers;
            }
            set
            {
                _PermissionRemainingUsers = value;
            }
        }
        private List<string> _PermissionRemainingUsers = null;


        private async Task UpdatePermission(PermissionPayload permission)
        {
            var updatePermissionRequest = new Web.Request(HttpMethod.Post, "/auth/addpermission", await GlobalAppState.GetAuthToken(), JsonConvert.SerializeObject(permission));
            _ = updatePermissionRequest.GetResultNoResponse();
        }

        private async Task DeletePermission(string UserName)
        {
            var deletePermissionRequest = new Web.Request(HttpMethod.Post, $"/auth/deletepermission/{UserName}", await GlobalAppState.GetAuthToken());
            _ = deletePermissionRequest.GetResultNoResponse();

            Permissions.RemoveAll(p => p.UserName == UserName);
            StateHasChanged();
        }

        private void OnPermissionUserSelectionClosed()
        {
            if (PermissionSelectedUsers != null)
            {
                foreach (var SelectedUser in PermissionSelectedUsers)
                {
                    PermissionPayload newUserPayload = new PermissionPayload();
                    newUserPayload.UserName = SelectedUser;
                    Permissions.Add(newUserPayload);
                }
                PermissionRemainingUsers = null;
                PermissionSelectedUsers = null;
                StateHasChanged();
            }
        }

        #endregion

        #region Groups
        private ITable GroupTable;

        private bool CreateOrEditGroupDialogVisible = false;
        private bool DeleteGroupDialogVisible = false;
        private string GroupId;
        private string GroupName;

        private IEnumerable<string> GroupUsers = new List<string>();

        private void ResetGroupValues()
		{
            GroupId = null;
            GroupName = null;
            GroupUsers = new List<string>();
        }

        private void OnClickGroupHubButton(GroupPayload group)
		{
            NavManager.NavigateTo($"/grouphub?Group={group.GroupModelId}-{group.GroupName}");
        }

        private void OnClickEdit(GroupPayload group)
		{
            GroupId = group.GroupModelId.ToString();
            GroupName = group.GroupName;
            GroupUsers = AllUsers.Intersect(group.GroupUserNames, StringComparer.OrdinalIgnoreCase).ToList(); // GroupUsers needs to match the case of AllUsers
            CreateOrEditGroupDialogVisible = true;
            StateHasChanged();
		}

        private void OnClickDelete(GroupPayload group)
		{
            GroupId = group.GroupModelId.ToString();
            GroupName = group.GroupName;
            DeleteGroupDialogVisible = true;
            StateHasChanged();
		}

        private async Task CreateOrEditGroupOnOk()
		{
            bool bInputValid = true;
            if (String.IsNullOrWhiteSpace(GroupName))
            {
                bInputValid = false;
                Toaster.Add("GroupName must contain something", MatToastType.Danger, "Group name");
            }

            if (GroupUsers == null || GroupUsers.Count() == 0)
			{
                bInputValid = false;
                Toaster.Add("No Users Specified", MatToastType.Danger, "Users");
            }

            if (bInputValid)
			{
                CreateGroupPayload createGroupPayload = new CreateGroupPayload
                {
                    GroupId = GroupId,
                    GroupName = GroupName,
                    GroupUsers = GroupUsers.ToList()
                };
                var createGroupRequest = new Web.Request(HttpMethod.Post, "/auth/creategroup", await GlobalAppState.GetAuthToken(), JsonConvert.SerializeObject(createGroupPayload));
                var response = await createGroupRequest.GetResultAsStringAsync();
                if (createGroupRequest.StatusCode == HttpStatusCode.OK)
				{
                    if (GroupId == null)
					{
                        Toaster.Add("Group Created", MatToastType.Success, "Success");
                    }
                    else
					{
                        Toaster.Add("Group Modified", MatToastType.Success, "Success");
                    }
                    
                    await RefreshGroups();
                    StateHasChanged();
				}
                else if (createGroupRequest.StatusCode == HttpStatusCode.NotAcceptable)
				{
                    Toaster.Add($"Create Group Failure: {response}", MatToastType.Danger, "Failure");
				}
                else
				{
                    Toaster.Add("Unknown error occurred", MatToastType.Danger, "???");
                }
            }

            ResetGroupValues();
            CreateOrEditGroupDialogVisible = false;
            await Task.Delay(500);
        }

        private async Task CreateOrEditGroupOnCancel()
		{
            ResetGroupValues();

            CreateOrEditGroupDialogVisible = false;
            await Task.Delay(500);
		}

        private async Task DeleteGroupOnOk()
		{
            var deleteGroupRequest = new Web.Request(HttpMethod.Post, $"/auth/deletegroup/{GroupId}", await GlobalAppState.GetAuthToken());
            await deleteGroupRequest.GetResultNoResponse();
            if (deleteGroupRequest.StatusCode == HttpStatusCode.OK)
			{
                Toaster.Add("Group Deleted", MatToastType.Success, "Success");
                await RefreshGroups();
                StateHasChanged();
			}
            else
			{
                Toaster.Add("Failed to delete group.  Unknown error occurred", MatToastType.Danger, "Unknown Error");
			}

            ResetGroupValues();
            DeleteGroupDialogVisible = false;
            await Task.Delay(500);
        }

        private async Task DeleteGroupOnCancel()
		{
            ResetGroupValues();
            DeleteGroupDialogVisible = false;
            await Task.Delay(500);
        }

        #endregion

        #region Cache Reset
        private async Task ResetCache()
        {
            await GlobalAppState.LS_Clear("Map_WorldSectorsCache");
            await GlobalAppState.LS_Clear("Map_SystemStarsCache");
            await GlobalAppState.LS_Clear("Map_SystemLinksCache");
            await GlobalAppState.LS_Clear("Map_PlanetResourcesCache");
            await GlobalAppState.LS_Clear("Map_AllPlanetMaterialsCache");

            Toaster.Add("Cache Cleared Successfully", MatToastType.Info, "Cache Cleared");
        }
        #endregion
    }
}