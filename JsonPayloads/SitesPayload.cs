using System;

namespace FIOWeb.JsonPayloads
{
    public class SitesPayload
    {
        public Site[] Sites { get; set; }
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class Site
    {
        public Building[] Buildings { get; set; }
        public string SiteId { get; set; }
        public string PlanetId { get; set; }
        public string PlanetIdentifier { get; set; }
        public string PlanetName { get; set; }
        public long PlanetFoundedEpochMs { get; set; }
    }

    public class Building
    {
        public Reclaimablematerial[] ReclaimableMaterials { get; set; }
        public Repairmaterial[] RepairMaterials { get; set; }
        public long BuildingCreated { get; set; }
        public string BuildingId { get; set; }
        public string BuildingName { get; set; }
        public string BuildingTicker { get; set; }
        public float Condition { get; set; }
    }

    public class Reclaimablematerial
    {
        public string MaterialId { get; set; }
        public string MaterialName { get; set; }
        public string MaterialTicker { get; set; }
        public int MaterialAmount { get; set; }
    }

    public class Repairmaterial
    {
        public string MaterialId { get; set; }
        public string MaterialName { get; set; }
        public string MaterialTicker { get; set; }
        public int MaterialAmount { get; set; }
    }

}
