using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

using Blazored.LocalStorage;
using Blazored.SessionStorage;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

using FIOWeb.Models;

namespace FIOWeb
{
    public enum LoadState
    {
        Loading = 0,
        Authenticating = 1,
        Authenticated = 2,
        NotAuthenticated = 3,
    }

    public class GlobalAppState
    {
        public event Action OnChange;

        // Need to keep these around since the localStorageService has a narrower scope than Singleton
        private readonly IServiceProvider serviceProvider;
        private readonly IServiceScope scope;

        private readonly ILocalStorageService localStorageService;
        private readonly ISessionStorageService sessionStorageService;

        public GlobalAppState(IServiceProvider sp)
        {
            serviceProvider = sp;
            scope = serviceProvider.CreateScope();
            localStorageService = (ILocalStorageService)scope.ServiceProvider.GetRequiredService(typeof(ILocalStorageService));
            sessionStorageService = (ISessionStorageService)scope.ServiceProvider.GetRequiredService(typeof(ISessionStorageService));
        }

        public async Task TryAuthCached()
        {
            if (await LSGet_SavedCredentials())
            {
                string userName = await LSGet_UserName();
                string password = await LSGet_Password();
                await Authenticate(userName, password, true);
            }
            else
            {
                State = LoadState.NotAuthenticated;
            }
        }

        public async Task Authenticate(string userName, string password, bool bRememberCredentials = false)
        {
            State = LoadState.Authenticating;
            var loginPayload = new JsonPayloads.JsonLoginPayload();
            loginPayload.UserName = userName;
            loginPayload.Password = password;

            Web.Request loginRequest = new Web.Request(HttpMethod.Post, "/auth/login", null, JsonConvert.SerializeObject(loginPayload));
            var loginResultPayload = await loginRequest.GetResponseAsync<JsonPayloads.JsonAuthLoginPayload>();
            if (loginResultPayload != null)
            {
                await LSSet_UserName(userName);
                if (bRememberCredentials)
                {
                    await LSSet_Password(password);
                }

                await LSSet_AuthToken(loginResultPayload.AuthToken);
                await LSSet_Expiry(loginResultPayload.Expiry);
                await LSSet_SavedCredentials(bRememberCredentials);
                await DetermineIfAdmin();
                await DeterminePermissionAllowances();
                State = LoadState.Authenticated;
            }
            else
            {
                await localStorageService.ClearAsync();
                State = LoadState.NotAuthenticated;
            }
        }

        public async Task LogOut()
        {
            await localStorageService.ClearAsync();
            State = LoadState.NotAuthenticated;
        }

        public LoadState State
        {
            get
            {
                return _State;
            }
            private set
            {
                if (_State != value)
                {
                    _State = value;
                    NotifyStateChanged();
                }
            }
        }
        private LoadState _State = LoadState.Loading;

        public bool IsAdmin
        {
            get; private set;
        } = false;

        #region PermissionAllowances
        private object PermissionAllowancesLockObj = new object();
        public List<PermissionAllowance> PermissionAllowances
        {
            get
            {
                lock (PermissionAllowancesLockObj)
                {
                    if (_PermissionAllowances != null)
                    {
                        return new List<PermissionAllowance>(_PermissionAllowances);
                    }

                    return null;
                }
            }
            private set
            {
                lock (PermissionAllowancesLockObj)
                {
                    _PermissionAllowances = value;
                }
            }
        }
        private List<PermissionAllowance> _PermissionAllowances = null;
        #endregion

        private async Task DetermineIfAdmin()
        {
            var checkAdminRequest = new Web.Request(HttpMethod.Get, "/admin", await GetAuthToken());
            await checkAdminRequest.GetResultNoResponse();
            IsAdmin = (checkAdminRequest.StatusCode == HttpStatusCode.OK);
        }

        private async Task DeterminePermissionAllowances()
        {
            var permAllowancesRequest = new Web.Request(HttpMethod.Get, "/auth/visibility", await GetAuthToken());
            PermissionAllowances = await permAllowancesRequest.GetResponseAsync<List<PermissionAllowance>>();
        }

        public async Task<string> GetUserName()
        {
            return await LSGet_UserName();
        }

        public async Task<string> GetAuthToken()
        {
            if (State == LoadState.Authenticated)
            {
                // Give us some buffer room
                DateTime now = DateTime.Now.ToUniversalTime().AddMinutes(-30);
                DateTime expiry = await LSGet_Expiry();
                if (now > expiry)
                {
                    if (await LSGet_SavedCredentials())
                    {
                        // Try and auth again
                        string userName = await LSGet_UserName();
                        string password = await LSGet_Password();
                        await Authenticate(userName, password, true);
                    }
                    else
                    {
                        State = LoadState.NotAuthenticated;

                    }
                }
            }

            return await LSGet_AuthToken();
        }

        #region LocalStorage - SavedCredentials
        private async Task LSSet_SavedCredentials(bool value)
        {
            await localStorageService.SetItemAsync("SavedCredentials", value);
        }

        private async Task<bool> LSGet_SavedCredentials()
        {
            if (await localStorageService.ContainKeyAsync("SavedCredentials"))
            {
                return await localStorageService.GetItemAsync<bool>("SavedCredentials");
            }

            return false;
        }
        #endregion

        #region LocalStorage - UserName
        private async Task LSSet_UserName(string value)
        {
            CachedUserName = value;
            await localStorageService.SetItemAsync("UserName", value);
        }

        private async Task<string> LSGet_UserName()
        {
            if (CachedUserName == null)
            {
                if (await localStorageService.ContainKeyAsync("UserName"))
                {
                    CachedUserName = await localStorageService.GetItemAsync<string>("UserName");
                }
            }

            return CachedUserName;
        }
        private string CachedUserName = null;
        #endregion

        #region LocalStorage - Password
        private async Task LSSet_Password(string value)
        {
            CachedPassword = value;
            await localStorageService.SetItemAsync("Password", value);
        }

        private async Task<string> LSGet_Password()
        {
            if (CachedPassword == null)
            {
                if (await localStorageService.ContainKeyAsync("Password"))
                {
                    CachedPassword = await localStorageService.GetItemAsync<string>("Password");
                }
            }

            return CachedPassword;
        }
        private string CachedPassword = null;
        #endregion

        #region LocalStorage - AuthToken
        private async Task LSSet_AuthToken(string value)
        {
            CachedAuthToken = value;
            await localStorageService.SetItemAsync("AuthToken", value);
        }

        private async Task<string> LSGet_AuthToken()
        {
            if (CachedAuthToken == null)
            {
                if (await localStorageService.ContainKeyAsync("AuthToken"))
                {
                    CachedAuthToken = await localStorageService.GetItemAsync<string>("AuthToken");
                }
            }

            return CachedAuthToken;
        }
        private string CachedAuthToken = null;
        #endregion

        #region LocalStorage - Expiry
        private async Task LSSet_Expiry(DateTime value)
        {
            CachedExpiry = value;
            await localStorageService.SetItemAsync("Expiry", value);
        }

        private async Task<DateTime> LSGet_Expiry()
        {
            if (CachedExpiry == DateTime.MinValue)
            {
                if (await localStorageService.ContainKeyAsync("Expiry"))
                {
                    CachedExpiry = await localStorageService.GetItemAsync<DateTime>("Expiry");
                }
            }

            return CachedExpiry;
        }
        private DateTime CachedExpiry = DateTime.MinValue;
        #endregion
        
        public async Task<bool> LS_ContainsKey(string Key)
        {
            return await localStorageService.ContainKeyAsync(Key);
        }

        public async Task LS_Clear(string Key)
        {
            if ( await localStorageService.ContainKeyAsync(Key))
            {
                await localStorageService.RemoveItemAsync(Key);
            }
        }

        public async Task LSSet_Generic<T>(string Key, T value)
        {
            await localStorageService.SetItemAsync(Key, value);
        }

        public async Task<T> LSGet_Generic<T>(string Key)
        {
            if (await localStorageService.ContainKeyAsync(Key))
            {
                return await localStorageService.GetItemAsync<T>(Key);
            }

            return default(T);
        }

        public async Task<bool> SS_ContainsKey(string Key)
        {
            return await sessionStorageService.ContainKeyAsync(Key);
        }

        public async Task SS_Clear(string Key)
        {
            if ( await sessionStorageService.ContainKeyAsync(Key))
            {
                await sessionStorageService.RemoveItemAsync(Key);
            }
        }

        public async Task SSSet_Generic<T>(string Key, T value)
        {
            await sessionStorageService.SetItemAsync(Key, value);
        }

        public async Task<T> SSGet_Generic<T>(string Key)
        {
            if (await sessionStorageService.ContainKeyAsync(Key))
            {
                return await sessionStorageService.GetItemAsync<T>(Key);
            }

            return default(T);
        }


        private void NotifyStateChanged() => OnChange?.Invoke();
    }
}