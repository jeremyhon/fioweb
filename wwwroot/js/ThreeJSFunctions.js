import * as THREE from '../js/three.module.js';
import { OrbitControls } from '../js/OrbitControls.js';

var container, clock, controls;
var camera, scene, renderer, mixer;

var raycaster = new THREE.Raycaster();
var mouse = new THREE.Vector2();

function animate() {
    requestAnimationFrame(animate);
    render();
}

function render() {
    var delta = clock.getDelta();
    if (mixer !== undefined) {
        mixer.update(delta);
    }

    renderer.render(scene, camera);
}

function loadScene(sceneDescription) {

    container = document.getElementById('threejscontainer');
    if (!container) {
        return;
    }

    scene = new THREE.Scene();
    scene.background = new THREE.Color(0x222222);

    camera = new THREE.PerspectiveCamera(70, container.clientWidth / container.clientHeight, 1.0, 6000);
    camera.up.set(0, 0, 1);
    camera.position.set(0, 0, 1500);
    
    clock = new THREE.Clock();

    const ambientLight = new THREE.AmbientLight(0xB0B0B0);
    scene.add(ambientLight);
    
    const directionalLight = new THREE.DirectionalLight(0x404040, 0.5);
    directionalLight.position.set(20, 100, 100);
    scene.add(directionalLight);

    for (var i = 0; i < sceneDescription.lines.length; i++) {
        var lineDesc = sceneDescription.lines[i];

        var material = null;
        if (lineDesc.dashed) {
            material = new THREE.LineDashedMaterial({ color: lineDesc.color, dashSize: 1, gapSize: 0.3 });
            material.linewidth = 0.25;
            material.transparent = true;
            material.opacity = 0.25;
        }
        else {
            material = new THREE.LineBasicMaterial({ color: lineDesc.color });
            material.linewidth = 0.5;
            material.transparent = true;
            material.opacity = 0.5;
        }

        var points = [];
        points.push(new THREE.Vector3(lineDesc.startPositionX, lineDesc.startPositionY, lineDesc.startPositionZ));
        points.push(new THREE.Vector3(lineDesc.endPositionX, lineDesc.endPositionY, lineDesc.endPositionZ));

        var geometry = new THREE.BufferGeometry().setFromPoints(points);
        var line = new THREE.Line(geometry, material);
        line.Attributes = undefined;
        scene.add(line);
    }

    for (var i = 0; i < sceneDescription.stars.length; i++) {
        var starDesc = sceneDescription.stars[i];

        var geometry = new THREE.SphereGeometry(starDesc.radius * 2.0, 32, 32); // Multiplying radius by 2 to make it easier to click
        var material = new THREE.MeshBasicMaterial({ color: starDesc.color }); // TODO getStarShaderMesh(starDesc.color); 
        var sphere = new THREE.Mesh(geometry, material);
        sphere.position.set(starDesc.positionX, starDesc.positionY, starDesc.positionZ);

        sphere.Attributes = starDesc;
        scene.add(sphere);
    }

    for (var i = 0; i < sceneDescription.connections.length; i++) {
        var conn = sceneDescription.connections[i];

        // @TODO: Figure out colors
        var material = new THREE.LineBasicMaterial({ color: 0x818181 });
        var points = [];
        points.push(new THREE.Vector3(conn.startPositionX, conn.startPositionY, conn.startPositionZ));
        points.push(new THREE.Vector3(conn.endPositionX, conn.endPositionY, conn.endPositionZ));

        var geometry = new THREE.BufferGeometry().setFromPoints(points);
        var line = new THREE.Line(geometry, material);
        line.Attributes = undefined;
        scene.add(line);
    }

    renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(container.clientWidth, container.clientHeight);


    while (container.lastElementChild) {
        container.removeChild(container.lastElementChild);
    }

    container.appendChild(renderer.domElement);

    controls = new OrbitControls(camera, renderer.domElement);
    controls.screenSpacePanning = true;
    controls.minDistance = 200;
    controls.maxDistance = 1500;
    controls.target.set(0, 2, 0);
    controls.update();

    animate();

    renderer.domElement.addEventListener('mousemove', onMouseOver, false);
    renderer.domElement.addEventListener('mousedown', onMouseDown, false);
}

// TODO Make star shaders work
//function starVertexShader() {
//    return `
//    varying vec3 vUv; 

//    void main() {
//      vUv = position;

//      vec4 modelViewPosition = modelViewMatrix * vec4(position, 1.0);
//      gl_Position = projectionMatrix * modelViewPosition;  
//    }
//  `
//}

//function starFragmentShader() {
//    return `
//      uniform vec3 starColor; 
//      uniform vec3 fadeColor; 
//      varying vec3 vUv;

//      void main() {
//        gl_FragColor = vec4(mix(starColor, fadeColor, vUv.z), 1.0);
//      }
//  `
//}

//function getStarShaderMesh(starColor) {
//    let uniforms = {
//        colorB: { type: 'vec3', value: new THREE.Color(starColor) },
//        colorA: { type: 'vec3', value: new THREE.Color(0x333333) }
//    }
//    return new THREE.ShaderMaterial({
//        uniforms: uniforms,
//        vertexShader: starVertexShader(),
//        fragmentShader: starFragmentShader(),
//    })
//}

function onMouseOver(event) {
    mouse.x = (event.offsetX / container.clientWidth) * 2 - 1;
    mouse.y = - (event.offsetY / container.clientWidth) * 2 + 1;
    raycaster.setFromCamera(mouse, camera);

    var intersects = raycaster.intersectObjects(scene.children, true);

    // if there is one (or more) intersections
    for (var i = 0; i < intersects.length; i++) {   
        var obj = intersects[i].object;
        if (obj.Attributes !== undefined) {
            var objOut = JSON.stringify(obj.Attributes);
            console.log(objOut);
            break;
        }
    }
}

function onMouseDown(event) {
    mouse.x = (event.offsetX / container.clientWidth) * 2 - 1;
    mouse.y = - (event.offsetY / container.clientWidth) * 2 + 1;
    raycaster.setFromCamera(mouse, camera);

    var intersects = raycaster.intersectObjects(scene.children, true);

    // if there is one (or more) intersections
    for (var i = 0; i < intersects.length; i++) {
        var obj = intersects[i].object;
        if (obj.Attributes !== undefined) {
            DotNetObjRef.invokeMethodAsync("SetSystemNaturalId", obj.Attributes.naturalId);
            break;
        }
    }
}

function onStart() {
    isPlayed = true;
}

function onStop() {
    isPlayed = false;
}

var DotNetObjRef = null;

window.ThreeJSFunctions = {
    load: ( sceneDescription ) => { loadScene(sceneDescription); },
    stop: () => { onStop(); },
    start: () => { onStart(); },
    setMapHandler: (dotNetObjRef) => { DotNetObjRef = dotNetObjRef;},
};

window.onload = loadScene;
