﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

using FIOWeb.JsonPayloads;

namespace FIOWeb.Models
{
    public enum BurnModelShow
    {
        ShowAll,
        ShowOnlyWorkforce,
        ShowOnlyProduction
    }

    public class BurnModel
    {
        public BurnModelShow Show
        {
            get
            {
                return _Show;
            }
            set
            {
                _Show = value;
                foreach (var child in Children)
                {
                    child.Show = value;
                }
            }
        }
        private BurnModelShow _Show = BurnModelShow.ShowAll;

        public List<string> ProducerUserNames { get; set; } = new List<string>();
        public List<string> ConsumerUserNames { get; set; } = new List<string>();

        public string PlanetName { get; set; }
        public string PlanetNaturalId { get; set; }
        public string PlanetDisplayName
        {
            get
            {
                return (PlanetName == PlanetNaturalId) ? PlanetName : $"{PlanetName} ({PlanetNaturalId})";
            }
        }

        public DateTime? PlanetConsumptionTime { get; set; }

        public DateTime? LastUpdate { get; set; }
        public string LastUpdateCause { get; set; }

        public string MaterialTicker { get; set; }

        public List<string> UserMaterialAmounts { get; set; } = new List<string>();
        public int MaterialAmount { get; set; } = 0;

        public List<string> UserWorkforceConsumptions { get; set; } = new List<string>();
        public double WorkforceConsumption { get; set; } = 0.0;

        public List<string> UserOrderConsumptions { get; set; } = new List<string>();
        public double OrderConsumption { get; set; } = 0.0;

        public List<string> UserOrderProductions { get; set; } = new List<string>();
        public double OrderProduction { get; set; } = 0.0;

        public double WorkforceDailyDelta { get; set; } = 0.0;
        public double OrderDailyDelta { get; set; } = 0.0;

        public double Delta
        {
            get
            {
                if (Show == BurnModelShow.ShowAll)
                {
                    return WorkforceDailyDelta + OrderDailyDelta;
                }
                else if (Show == BurnModelShow.ShowOnlyProduction)
                {
                    return OrderDailyDelta;
                }
                else
                {
                    return WorkforceDailyDelta;
                }
            }
        }

        public string UnitsAvailStr
        {
            get
            {
                if (MaterialAmount > 0)
                {
                    return $"{MaterialAmount}";
                }

                return string.Empty;
            }
        }

        public string BurnStr
        {
            get
            {
                double Burn = 0.0;
                if (Show == BurnModelShow.ShowAll)
                {
                    Burn = WorkforceConsumption + OrderConsumption;
                }
                else if (Show == BurnModelShow.ShowOnlyWorkforce)
                {
                    Burn = WorkforceConsumption;
                }
                else
                {
                    Burn = OrderConsumption;
                }

                if (Burn > 0.0)
                {
                    return $"{Burn:N2}/d";
                }

                return string.Empty;
            }
        }

        public string ProduceStr
        {
            get
            {
                double Produce = 0.0;
                if (Show != BurnModelShow.ShowOnlyWorkforce)
                {
                    Produce = OrderProduction;
                }

                if (Produce > 0.0)
                {
                    return $"{Produce:N2}/d";
                }

                return string.Empty;
            }
        }

        public string OverallStr
        {
            get
            {
                if (Delta != 0.0)
                {
                    return $"{Delta:N2}/d";
                }

                return string.Empty;
            }
        }

        public double DaysUntilExhaustion
        {
            get
            {
                if (Delta < 0)
                {
                    return (double)MaterialAmount / -Delta;
                }
                else
                {
                    // ∞
                    return double.PositiveInfinity;
                }
            }
        }

        public string DaysUntilExhaustionStr
        {
            get
            {
                if (Children.Count > 0)
                {
                    var minimum = Children.Min(c => c.DaysUntilExhaustion);
                    if (minimum != double.PositiveInfinity)
                    {
                        return $"{minimum:N2}d";
                    }

                    return string.Empty;
                }
                else
                {
                    if (DaysUntilExhaustion == double.PositiveInfinity)
                    {
                        return "∞";
                    }

                    return $"{DaysUntilExhaustion:N2}d";
                }
            }
        }

        public List<string> AllPlanetUsers
        {
            get
            {
                if (_AllPlanetUsers == null)
                {
                    _AllPlanetUsers = new List<string>();
                    if (Children.Count > 0)
                    {
                        foreach (var child in Children)
                        {
                            foreach (var consumer in child.ConsumerUserNames)
                            {
                                if (!_AllPlanetUsers.Contains(consumer))
                                {
                                    _AllPlanetUsers.Add(consumer);
                                }
                            }

                            foreach (var producer in child.ProducerUserNames)
                            {
                                if (!_AllPlanetUsers.Contains(producer))
                                {
                                    _AllPlanetUsers.Add(producer);
                                }
                            }
                        }
                    }
                    _AllPlanetUsers = _AllPlanetUsers.OrderBy(apu => apu).ToList();
                }

                return _AllPlanetUsers;
            }
        }
        private List<string> _AllPlanetUsers = null;

        public string AllPlanetUsersStr
        {
            get
            {
                return string.Join(", ", AllPlanetUsers);
            }
        }

        public string ConsumerUsersStr
        {
            get
            {
                return string.Join(", ", ConsumerUserNames);
            }
        }

        public string ProductionUsersStr
        {
            get
            {
                return string.Join(", ", ProducerUserNames);
            }
        }

        public BurnModel Parent { get; set; } = null;

        public List<BurnModel> Children { get; set; } = new List<BurnModel>();

        public BurnModel()
        {

        }

        public BurnModel(BurnModel parent)
        {
            Parent = parent;
        }

        public static async Task<List<BurnModel>> GetBurnModels(string ActiveUserName, List<string> UserNames, int GroupId, string AuthToken, List<MaterialPayload> AllMaterials)
        {
            var burnModels = new List<BurnModel>();

            Web.Request burnRequest;
            if (ActiveUserName != null)
            {
                burnRequest = new Web.Request(HttpMethod.Get, $"/fioweb/burn/user/{ActiveUserName}", AuthToken);
            }
            else if (UserNames != null)
            {
                var userNamesSeparated = string.Join("__", UserNames);
                burnRequest = new Web.Request(HttpMethod.Get, $"/fioweb/burn/users/{userNamesSeparated}", AuthToken);
            }
            else
            {
                Debug.Assert(GroupId >= 0);
                burnRequest = new Web.Request(HttpMethod.Get, $"/fioweb/burn/group/{GroupId}", AuthToken);
            }

            var burnResults = await burnRequest.GetResponseAsync<List<BurnPayload>>();
            if (burnResults == null)
            {
                return burnModels;
            }

            foreach (var burnResult in burnResults)
            {
                // @TODO: Handle failures
                if (burnResult.Error != null)
                {
                    continue;
                }

                var UserName = burnResult.UserName;

                var planet = burnModels.FirstOrDefault(br => br.PlanetNaturalId == burnResult.PlanetNaturalId);
                if (planet == null)
                {
                    planet = new BurnModel();
                    burnModels.Add(planet);
                }
                planet.PlanetName = burnResult.PlanetName;
                planet.PlanetNaturalId = burnResult.PlanetNaturalId;
                planet.PlanetConsumptionTime = burnResult.PlanetConsumptionTime;
                planet.LastUpdate = burnResult.LastUpdate;
                planet.LastUpdateCause = burnResult.LastUpdateCause;

                foreach (var workforceConsumption in burnResult.WorkforceConsumption)
                {
                    var burnItem = planet.Children.FirstOrDefault(c => c.MaterialTicker == workforceConsumption.MaterialTicker);
                    if (burnItem == null)
                    {
                        burnItem = new BurnModel(planet);
                        planet.Children.Add(burnItem);
                    }
                    burnItem.ConsumerUserNames.Add(UserName);
                    burnItem.MaterialTicker = workforceConsumption.MaterialTicker;

                    burnItem.UserWorkforceConsumptions.Add($"{UserName}: {workforceConsumption.DailyAmount:N2}/d");
                    burnItem.WorkforceConsumption += workforceConsumption.DailyAmount;
                    burnItem.WorkforceDailyDelta -= workforceConsumption.DailyAmount;
                }

                foreach (var orderConsumption in burnResult.OrderConsumption)
                {
                    var burnItem = planet.Children.FirstOrDefault(c => c.MaterialTicker == orderConsumption.MaterialTicker);
                    if (burnItem == null)
                    {
                        burnItem = new BurnModel(planet);
                        planet.Children.Add(burnItem);
                    }
                    burnItem.ConsumerUserNames.Add(UserName);
                    burnItem.MaterialTicker = orderConsumption.MaterialTicker;
                    burnItem.UserOrderConsumptions.Add($"{UserName}: {orderConsumption.DailyAmount:N2}/d");
                    burnItem.OrderConsumption += orderConsumption.DailyAmount;
                    burnItem.OrderDailyDelta -= orderConsumption.DailyAmount;
                }

                foreach (var orderProduction in burnResult.OrderProduction)
                {
                    var burnItem = planet.Children.FirstOrDefault(c => c.MaterialTicker == orderProduction.MaterialTicker);
                    if (burnItem == null)
                    {
                        burnItem = new BurnModel(planet);
                        planet.Children.Add(burnItem);
                    }
                    burnItem.ProducerUserNames.Add(UserName);
                    burnItem.MaterialTicker = orderProduction.MaterialTicker;
                    burnItem.UserOrderProductions.Add($"{UserName}: {orderProduction.DailyAmount:N2}/d");
                    burnItem.OrderProduction += orderProduction.DailyAmount;
                    burnItem.OrderDailyDelta += orderProduction.DailyAmount;
                }

                foreach (var burnItem in planet.Children)
                {
                    var inventoryItem = burnResult.Inventory.FirstOrDefault(i => i.MaterialTicker == burnItem.MaterialTicker);
                    if (inventoryItem != null)
                    {
                        burnItem.UserMaterialAmounts.Add($"{UserName}: {inventoryItem.MaterialAmount}");
                        burnItem.MaterialAmount += inventoryItem.MaterialAmount;
                    }
                }
            }

            burnModels = burnModels.OrderBy(bm => bm.PlanetNaturalId).ToList();

            if (burnModels.Count > 1)
            {
                // Add the overall last
                var overallModel = new BurnModel();
                overallModel.PlanetNaturalId = "Overall";
                overallModel.PlanetName = "Overall";
                foreach (var Child in burnModels.SelectMany(bm => bm.Children))
                {
                    var ChildModel = overallModel.Children.FirstOrDefault(c => c.MaterialTicker == Child.MaterialTicker);
                    if (ChildModel == null)
                    {
                        ChildModel = new BurnModel();
                        overallModel.Children.Add(ChildModel);
                    }

                    ChildModel.MaterialTicker = Child.MaterialTicker;
                    ChildModel.MaterialAmount += Child.MaterialAmount;

                    ChildModel.WorkforceConsumption += Child.WorkforceConsumption;
                    ChildModel.OrderConsumption += Child.OrderConsumption;
                    ChildModel.OrderProduction += Child.OrderProduction;

                    ChildModel.WorkforceDailyDelta += Child.WorkforceDailyDelta;
                    ChildModel.OrderDailyDelta += Child.OrderDailyDelta;
                }

                burnModels.Add(overallModel);
            }

            foreach (var burnModel in burnModels)
            {
                burnModel.Children = burnModel.Children.OrderBy(c => c.MaterialTicker).ToList();
            }

            return burnModels;
        }
    }
}
