namespace FIOWeb.Models
{
    public class PermissionAllowance
    {
        public string UserName { get; set; }

        public bool FlightData { get; set; }
        public bool BuildingData { get; set; }
        public bool StorageData { get; set; }
        public bool ProductionData { get; set; }
        public bool WorkforceData { get; set; }
        public bool ExpertsData { get; set; }
    }
}