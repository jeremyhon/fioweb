using System.Threading.Tasks;

using MatBlazor;
using Microsoft.AspNetCore.Components.Web;

namespace FIOWeb.Shared
{
    public partial class NavMenu
    {
        private bool collapseNavMenu = true;

        private string NavMenuCssClass => collapseNavMenu ? "collapse" : null;

        private void ToggleNavMenu()
        {
            collapseNavMenu = !collapseNavMenu;
        }

        protected override void OnInitialized()
        {
            GlobalAppState.OnChange += StateHasChanged;
            _ = GlobalAppState.TryAuthCached();
        }

        public void Dispose()
        {
            GlobalAppState.OnChange -= StateHasChanged;
        }

        string loginInputUserName;
        string loginInputPassword;
        bool loginRemember;

        void ToggleChecked(bool value)
        {
            loginRemember = value;
        }

        bool loginDlgVisible = false;

        private async Task OnLoginOk(MouseEventArgs e)
        {
            loginDlgVisible = false;
            await Task.Delay(500);
            await GlobalAppState.Authenticate(loginInputUserName, loginInputPassword, loginRemember);
            if (GlobalAppState.State == LoadState.Authenticated)
            {
                Toaster.Add("Logged in", MatToastType.Success, "Authentication Success");
            }
            else
            {
                Toaster.Add("Failed to log in", MatToastType.Danger, "Authentication Failure");
            }
        }

        private async Task OnLoginCancel(MouseEventArgs e)
        {
            loginDlgVisible = false;
            await Task.Delay(500);
        }

        private async Task OnLogout(MouseEventArgs e)
        {
            await GlobalAppState.LogOut();
        }
    }
}